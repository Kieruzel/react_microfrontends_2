import React, { useEffect, useRef } from "react";
import ReactDOM from "react-dom/client";
import { EventEmitter } from "eventemitter3";
import RemoteComponent from "remote/Remote";
import RemoteDecoupled from "remote/RemoteDecoupled";
import { BrowserRouter, Routes, Route, Link, useNavigate } from "react-router-dom";

const emitter = new EventEmitter();

const root = ReactDOM.createRoot(document.getElementById("root"));

const App = () => {
  const [counter, setCounter] = React.useState(0);
  const navigator = useNavigate();

  const ref = useRef(null);

  useEffect(() => {
    emitter.on("increment", (value) => {
      setCounter((prev) => prev + value);
    });

    emitter.on("decrement", (value) => {
      setCounter((prev) => prev - value);
    });

    return () => {
      emitter.off("increment");
      emitter.off("decrement");
    };
  }, []);

  useEffect(() => {
    if (ref.current) {
      RemoteDecoupled(ref.current);
    }
  });

  const increse = () => {
    setCounter((prev) => prev + 1);
  };

  console.log("App", emitter);

  return (
    <div>
      <p>Host / Shell</p>
      <RemoteComponent increse={increse} emitter={emitter} navigator={navigator}
      
      isLogedIn={true}
      
      />

      <h3>Count: {counter}</h3>
      <div ref={ref}></div>
    </div>
  );
};

root.render(
  <BrowserRouter>
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/remote">Remote</Link>
      </li>
    </ul>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="/remote" element={<RemoteComponent />} />
    </Routes>
  </BrowserRouter>
);
