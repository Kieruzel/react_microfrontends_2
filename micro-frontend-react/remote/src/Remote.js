import React from "react";
import styles from "./Remote.module.css";

const RemoteComponent = ({ increse, emitter, navigator }) => {
  console.log("RemoteComponent", emitter);

  return (
    <div className={styles.test}>
      <h1>I'm remote component</h1>

      <button onClick={() => {
        if (navigator) navigator("/remote");
      }}>Go to remote route</button>

      <button
        onClick={() => {
          if (typeof increse === "function") {
            increse();
          }
        }}
      >
        Increse count
      </button>

      <button
        onClick={() => {
          if (emitter) emitter.emit("increment", 10);
        }}
      >
        Increment count with emitter
      </button>

      <button
        onClick={() => {
          if (emitter) emitter.emit("decrement", 10);
        }}
      >
        Decrement count with emitter
      </button>
    </div>
  );
};

export default RemoteComponent;
